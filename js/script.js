 $(document).ready(() => {

 $(function() {

     $('ul.tabs').on('click', 'li:not(.active-tab)', function() {
         $(this)
             .addClass('active-tab').siblings().removeClass('active-tab')
             .$($('div.our-service-article').removeClass('our-service-article_active-div').eq($(this).index()).addClass('our-service-article_active-div'));
     });

 });


    $('#loadMore').on('click', function (event) {
        event.preventDefault(); //УБИРАЕМ ПЕРЕХОД ПО ССЫЛКЕ
        $('.amazing-img-div-hidden').css('display','block'); //ДЕЛАЕМ ВСЕ НАШИ "СПРЯТАННЫЕ" БЛОКИ С КАРТИНКАМИ ВИДИМЫМИ

        $('#loadMore').css({'background-color':'transparent', /////
                            'margin':'0 auto 65px',           /////
                            'width':'180px',                  /////
                            'height':'inherit',               ///// МЕНЯЕМ СТИЛИ КНОПКЕ КОТОРАЯ БЫЛА "LOAD ALL PICS"
                            'color':'#4e4e4e',                /////
                            'font-size':'16px',               /////
                            'font-weight':'700',              /////
                            'cursor':'default'}).text('All pictures uploaded');////И ДЕЛАЕМ ИЗ НЕЕ ТЕКС С СООБЩЕНИЕМ ЧТО ВСЕ КАРТИНКИ ЗАГРУЖЕННЫ

    });


    $('.amazing-work-tabs-item:eq(3)').on('click', function () { //ПО КЛИКУ НА "GRAPHIC DESIGN" ДЕЛАЕМ ВИДИМЫМИ ДВА БЛОКА ИЗ СЕКЦИИ "LANDING PAGE"
        $('#amazing-img-div-landing-will-show-first').css('display','block'); // И "WORDPRESS", ЧТОБЫ ОНИ СРАЗУ ОТОБРАЖАЛИСЬ В СВОИХ ВКЛАДКАХ
        $('#amazing-img-div-wordpress-will-show-first').css('display','block');

    })

     $('.amazing-work-tabs-item').click(function() {

         $(this)
             .addClass('active-amazing-tab').siblings().removeClass('active-amazing-tab');

         imgWidth = '52px';
         thisItem 	= $(this).attr('rel');

         if(thisItem !== 'all') {

             $('.amazing-img-div[rel='+thisItem+']')
                 .animate({'width' : '285px',
                                     'opacity' : 1,
                 });

             $('.amazing-img-div[rel!='+thisItem+']')
                 .animate({'width' : 0,
                                     'opacity' : 0,
                 });
         } else {

             $('.amazing-img-div')
                 .animate({'opacity' : 1,
                                     'width' : '285px',
                 });
         };
     });

        ///SLICK START///

     $('.for-slick').slick({
         variableHeight: true,
         arrows: false,
         dots: false,
     });

     $('.say-about-ham-tabs').slick({
         variableWidth: true,
         variableHeight: true,
         arrows: true,
         dots: false,
         slidesToShow: 4,
         slidesToScroll: 1,
         asNavFor: '.for-slick',
         focusOnSelect: true,
         // centerMode: true,
         infinite: true,
         prevArrow: '.fa-chevron-left',
         nextArrow: '.fa-chevron-right'
     });
        ///SLICK END///


     ///MASONRY START///

     let galleryContainer = $('.images-mas-wrapper');

     galleryContainer.imagesLoaded(function () {
         galleryContainer.masonry({
             columnWidth: 50,
             itemSelector: '.mas-imgs, .mass-lil-wrap',
             gutter:15,
             resize: true,
             fitWidth: true,
             // originLeft: false
             // initLayout: false
             // persentPosition:true
         });
     });

     $('#loadMoreMasonry').on('click',function (event) {
         event.preventDefault();
         $('.mas-imgs-hidden').css('display','block');
         $('#loadMoreMasonry').remove();
         galleryContainer.masonry()
     });

     ///MASONRY END///

    });



 // $('.amazing-work-tabs-item:eq(1)').on('click', function () {
 //     $('#gogi').css('display','block')
 //     $('#gogi').css('display','none')
 //
 // })

 // $('#gogi').css('display','block')
